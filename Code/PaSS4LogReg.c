/**
 * Particle Swarm Stepwise (PaSS) Algorithm for Logistic Regression
 * PaSS4LogReg.c
 * The main functions
 */

/**@mainpage
 * @author Mu Yang
 */

/**
 * Notation:
 * X     : the regressors
 * Y     : the regressand
 * Beta  : the effects
 * P     : the probability of Y=1
 * Theta : the logit function of P
 * lv    : the likelihood value
 * llv   : the log-likelihood value
 *
 * ================================================================
 *
 * Logistic model:
 * P     := exp(X*Beta) ./ 1+exp(X*Beta)
 * Theta := logit(P) = X*Beta
 *
 * Update Beta:
 * Theta := X * Beta
 * Eta   := exp(Theta)
 * P     := Eta ./ (1+Eta)
 * 1-P    = 1 ./ (1+Eta)
 * W     := P .* (1-P)
 * Beta  += inv( X'*diag(W)*X ) * X' * (Y-P)
 *
 * Compute log-likelihood:
 * lv    := prod( p^y * (1-p)^(1-y) )
 * llv   := log( lv )
 *        = sum( y * log(p) + (1-y) * log(1-p) )
 *        = sum( y * log(eta) - y * log(1+eta) - (1-Y) * log(1+eta) )
 *        = Y' * Theta - sum( log(1+eta) )
 *
 * Newton-Raphson method:
 * d(llv)/d(Beta)     =   X' * (Y-P)
 * d^2(llv)/d(Beta)^2 = - X' * diag(W) * X
 *
 * ================================================================
 *
 * Select index in forward step:
 * idx = argmax_{i not in I} llv_hat
 * Theta_hat := Theta_new - Theta = Beta[i] * X[i col]
 * Eta_hat   := Eta_new  ./ Eta   = exp( Theta_hat )
 * llv_hat   := llv_new - llv
 *            = ( Y' * Theta_new - Y' * Theta ) - ( sum( log(1+eta_new) ) - sum( log(1+eta) ) )
 *            = Y' * Theta_hat - sum( log( (1+eta_new)/(1+eta) ) )
 *            = Y' * Theta_hat - sum( log( 1 + (eta_hat-1)*p ) )
 *
 * Approximate beta[i] with Newton-Raphson method:
 * beta[i]   += ( X[i col]'*(Y-P_new) ) / ( X[i col]'*diag(W_new)*X[i col] )
 *
 * ================================================================
 *
 * Select index in backward step:
 * idx = argmax_{i in I} llv_hat
 * Theta_hat := Theta_new - Theta = -Beta[i] * X[i col]
 * Eta_hat   := Eta_new  ./ Eta   = exp( Theta_hat )
 * llv_hat   := llv_new - llv
 *            = ( Y' * Theta_new - Y' * Theta ) - ( sum( log(1+eta_new) ) - sum( log(1+eta) ) )
 *            = Y' * Theta_hat - sum( log( (1+eta_new)/(1+eta) ) )
 *            = Y' * Theta_hat - sum( log( 1 + (eta_hat-1)*p ) )
 *
 * ================================================================
 */

/**
 * References:
 *
 * 1. Compute Beta
 * Logistic Regression Parameter Estimation @ Zhen Liu & Meng Liu
 *
 * 2. Forward Selection
 * Parallel Large Scale Feature Selection for Logistic Regression Sameer Singh @ Sameer Singh & Jeremy Kubica & Scott Larsen & Daria Sorokina
 *
 * 3. Backward Selection
 * Feature Selection with Annealing for Regression, Classification and Ranking @ Adrian Barbu & Yiyuan She & Liangjing Ding & Gary Gramajo
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mkl.h>
#include <omp.h>

#define passSeed time( NULL )


/**
 * The criterion enumeration
 */
enum Criterion {
	AIC,   /**< Akaike information criterion */
	BIC,   /**< Bayesian information criterion */
	EBIC,  /**< Extended Bayesian information criterion */
	HDBIC, /**< High-dimensional Bayesian information criterion */
	HDHQ   /**< High-dimensional Hannan-Quinn information criterion */
};


/**
 * The parameter structure
 */
struct Parameter {
	int nP;    /**< the number of particles */
	int nI;    /**< the number of iterations */
	float pfg; /**< the probability for forward step: global */
	float pfl; /**< the probability for forward step: local */
	float pfr; /**< the probability for forward step: random */
	float pbl; /**< the probability for backward step: local */
	float pbr; /**< the probability for backward step: random */
};


/**
 * The data structure
 */
struct Data {
	float *X;       /**< matrix, n by (k+1), the regressors */
	float *Y;       /**< vector, n by 1, the regressand */
	float *Beta;    /**< vector, (k+1) by 1, the effects */
	float *Theta;   /**< vector, (k+1) by 1, X*Beta, the logit function of P */
	float *Eta;     /**< vector, (k+1) by 1, exp( Theta ) */
	float *P;       /**< vector, n by 1, Eta ./ (1+Eta), the problity of Y=1 */
	float *W;       /**< vector, n by 1, P.*(1-P) */
	float *M;       /**< matrix, (k+1) by (k+1), X'*diag(W)*X, lower packed storage */
	float *Ones;    /**< vector, n by 1, ones */
	float llv;      /**< scalar, the log-likelihood value */
	float phi;      /**< scalar, the value given by criterion */
	float phi_old;  /**< scalar, the value given by criterion of past iteration */
	float phi_best; /**< scalar, the value given by criterion, best solution */

	int *Idx_lf;    /**< vector, (k+1) by 1, map local effects to full effects */
	int *Idx_fl;    /**< vector, (p+1) by 1, map full effects to local effects */
	int *I;         /**< vector, p by 1, the chosen effects (boolean) */
	int *I_best;    /**< matrix, p by 1, the chosen effects (boolean), best solution */
	int k;          /**< scalar, the number of chosen effects */

	int stat;       /**< boolean, the status, 1: forward step, 0: backward step */

	float *Ftemp;   /**< vector, n by 1 */
	int *Itemp;     /**< vector, p by 1 */
};


/**
 * The report structure
 */
struct Report {
	int nC;    /**< the number of correct */
	int nPS;   /**< the number of positive selection */
	int nFD;   /**< the number of false discovery */
	float CR;  /**< the correct rate */
	float ICR; /**< the incorrect rate */
	float PSR; /**< the positive selection rate */
	float FDR; /**< the false discovery rate */
};


// Global variables
int n;                /**< scalar, the number of statistical units */
int p;                /**< scalar, the number of effects */
float *X;             /**< matrix, n by p, the regressors */
float *Y;             /**< vector, n by 1, the regressand */
int *J;               /**< matrix, p by 1, the chosen effects (boolean), true solution */
float ebic_gamma;     /**< scalar, a penalty parameter in EBIC */
enum Criterion cri;   /**< the criterion */
struct Parameter par; /**< the parameter */
char *dataname;       /**< string, the name of data */
int nT;               /**< the number of tests */
int nthr;             /**< the number of threads */

const float fNaN = 0.0f/0.0f;


// Functions
void pass_cfg( const char* );
void pass_load( const char* );
void pass_malloc_data( struct Data* );
void pass_free_data( struct Data* );
void pass_main( struct Data* );
void pass_init_model( struct Data* );
void pass_select_fb( struct Data* );
void pass_update_model( struct Data*, const int );
void pass_compute_beta( struct Data* );
void pass_compute_cri( struct Data* );


/**
 * Main function
 */
int main( int argc, char** argv ) {
	int i, j, k, t, totalCorrect, totalChoose, isize, ione = 1, itemp;
	float ftemp;
	double start_time, total_time = 0.0, compute_time;
	char *cfgroot, *dataroot, *cristr;
	struct Report report;
	struct Data *data, tdata, *bdata;

	/*================================================================*
	 * Set parameters and load data                                   *
	 *================================================================*/

	par.nP  = 32;
	par.nI  = 128;

	par.pfg = 0.1f;
	par.pfl = 0.8f;
	par.pfr = 0.1f;
	par.pbl = 0.9f;
	par.pbr = 0.1f;

	cri = EBIC;
	ebic_gamma = 1.0f;

	nT = 10;
	nthr = 0;

	cfgroot = "PaSS4LogReg.cfg";
	dataroot = "PaSS4LogReg.dat";
	dataname = "";

	printf("\n================================\n\n");

	if( argc > 1 ) cfgroot  = argv[1];
	if( argc > 2 ) dataroot = argv[2];

	// Load config
	pass_cfg( cfgroot );

	// Check parameter
	if( par.nP <= 0 ) {
		printf( "nP must be positive!\n" );
		exit( 1 );
	}
	if( par.nI <= 0 ) {
		printf( "nI must be positive or zero!\n" );
		exit( 1 );
	}
	if( nT < 0 ) {
		printf( "nT must be positive or zero!\n" );
		exit( 1 );
	}

	// Set criterion
	switch(cri) {
	case AIC: 
		cristr = "AIC";
		break;
	case BIC:
		cristr = "BIC";
		break;
	case EBIC:
		cristr = "EBIC";
		break;
	case HDBIC:
		cristr = "HDBIC";
		break;
	case HDHQ:
		cristr = "HDHQ";
		break;
	}

	// Set threads
	itemp = omp_get_max_threads();
	if( nthr > itemp || nthr <= 0 ) {
		nthr = itemp;
	}
	if( nthr > par.nP ) {
		nthr = par.nP;
	}
	printf( "Uses %d threads.\n", nthr );

	// Initialize Random Seed
	srand( passSeed );

	// Load data
	pass_load( dataroot );
	if( cri == EBIC ) {
		printf( "%s: n=%d, p=%d, nP=%d, nI=%d, nT=%d, cri=%s, gamma=%.1f\n\n", dataname, n, p, par.nP, par.nI, nT, cristr, ebic_gamma );
	}
	else {
		printf( "%s: n=%d, p=%d, nP=%d, nI=%d, nT=%d, cri=%s\n\n", dataname, n, p, par.nP, par.nI, nT, cristr );
	}

	// /*================================================================*
	//  * Centralize and Normalize the original data                     *
	//  *================================================================*/

	// // Centralize X
	// for( i = 0; i < p; i++ ) {
	// 	ftemp = 0.0f;
	// 	for( j = 0; j < n; j++ ) {
	// 		ftemp += X[i*n+j];
	// 	}
	// 	ftemp /= n;
	// 	for( j = 0; j < n; j++ ) {
	// 		X[i*n+j] -= ftemp;
	// 	}
	// }

	// // Normalize X
	// for( i = 0; i < p; i++ ) {
	// 	ftemp = 1.0f / snrm2( &n, X + i*n, &ione );
	// 	sscal( &n, &ftemp, X + i*n, &ione );
	// }

	/*================================================================*
	 * Initialize the program                                         *
	 *================================================================*/

	// Allocate memory
	data = (struct Data*) malloc( sizeof(struct Data) * par.nP );
	for( i = 0; i < par.nP; i++ ) {
		pass_malloc_data( data+i );
	}

	// Initialize report
	report.nC = 0;
	report.nPS = 0;
	report.nFD = 0;
	totalCorrect = 0;
	totalChoose = 0;

	// Set output size
	isize = (int) log10(p) + 1;

	/*================================================================*
	 * Create true model                                              *
	 *================================================================*/

	pass_malloc_data( &tdata );

	// Build true model
	pass_init_model( &tdata );
	for( i = 0; i < p; i++ ) {
		if( J[i] ) {
			pass_update_model( &tdata, i );
		}
	}
	pass_compute_beta( &tdata );
	pass_compute_cri( &tdata );

	// Display model
	printf( "True:\t%12.6f:\t", tdata.phi );
	for( i = 0; i < p; i++ ) {
		if( tdata.I[i] ) {
			totalCorrect++;
			printf( "%-*d ", isize, i );
		}
	}
	printf( "\n\n" );


	/*================================================================*
	 * Run PaSS                                                       *
	 *================================================================*/

	for( t = 0; t < nT; t++ ) {
		printf( "%4d:\t", t );

		// Run PaSS
		start_time = omp_get_wtime();
		pass_main( data );
		compute_time = omp_get_wtime() - start_time;
		total_time += compute_time;

		// Find best model
		ftemp = fNaN;
		for( i = 0; i < par.nP; i++ ) {
			if( !(data[i].phi_best > ftemp) ) {
				bdata = data+i;
				ftemp = bdata->phi_best;
			}
		}

		// Build best model
		pass_init_model( &tdata );
		for( i = 0; i < p; i++ ) {
			if( bdata->I_best[i] ) {
				pass_update_model( &tdata, i );
			}
		}
		pass_compute_beta( &tdata );
		pass_compute_cri( &tdata );

		// Display model
		printf( "%12.6f;\t", tdata.phi );
		for( i = 0; i < p; i++ ) {
			if( tdata.I[i] ) {
				printf( "%-*d ", isize, i );
			}
		}
		printf( "\n" );

		// Check accuracy
		itemp = 1;
		for( i = 0; i < p; i++ ) {
			if( tdata.I[i] && J[i] ) {
				report.nPS++;
				totalChoose++;
			}
			else if( tdata.I[i] ) {
				itemp = 0;
				report.nFD++;
				totalChoose++;
			}
			else if( J[i] ) {
				itemp = 0;
			}
		}
		report.nC += itemp;
	}


	/*================================================================*
	 * Make report                                                    *
	 *================================================================*/

	// Compute accuracy rate
	totalCorrect *= nT;
	report.CR  = (float) report.nC / nT;
	report.ICR = 1 - report.CR;
	report.PSR = (float) report.nPS / totalCorrect;
	report.FDR = (float) report.nFD / totalChoose;

	// Display report
	printf("\n================================\n\n");
	printf( "%s\n", dataname );
	printf( "nP    = %d\n", par.nP );
	printf( "nI    = %d\n", par.nI );
	printf( "pfg   = %.2f\n", par.pfg );
	printf( "pfl   = %.2f\n", par.pfl );
	printf( "pfr   = %.2f\n", par.pfr );
	printf( "pbl   = %.2f\n", par.pbl );
	printf( "pbr   = %.2f\n", par.pbr );
	printf( "cri   = %s\n", cristr );
	if( cri == EBIC ) {
		printf( "gamma = %.2f\n", ebic_gamma );
	}
	printf( "nT    = %d\n", nT );
	printf( "CR    = %.6f\n", report.CR );
	printf( "ICR   = %.6f\n", report.ICR );
	printf( "PSR   = %.6f\n", report.PSR );
	printf( "FDR   = %.6f\n", report.FDR );
	printf( "Time  = %.6lf sec\n", total_time / nT );
	printf("\n================================\n\n");

	/*================================================================*
	 * Free memory                                                    *
	 *================================================================*/

	// Free memory
	for( i = 0; i < par.nP; i++ ) {
		pass_free_data( data+i );
	}
	pass_free_data( &tdata );
	free( X );
	free( Y );
	free( J );
	free( data );

	/*================================================================*/

	return 0;
}


/**
 * Load config from file
 *
 * @param fileroot the root of config file
 */
void pass_cfg( const char* fileroot ) {
	#define BUF_SIZE 1024
	FILE *file;
	int offset1, offset2;
	char line[BUF_SIZE], *cristr;

	printf( "Loading config from '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "r" );

	// Check if file exists
	if( file ) {
		// Read data
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d %d", &par.nP, &par.nI );
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %f %f %f %f %f", &par.pfg, &par.pfl, &par.pfr, &par.pbl, &par.pbr );

		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %n %*s %n", &offset1, &offset2 );
		cristr = (char*) malloc( sizeof(char) * offset2-offset1 );
		sscanf( line, "%*s %s", cristr );
		if( !strcmp( cristr, "AIC" ) ) {
			cri = AIC;
		}
		else if( !strcmp( cristr, "BIC" ) ) {
			cri = BIC;
		}
		else if( !strcmp( cristr, "EBIC" ) ) {
			cri = EBIC;
			ebic_gamma = 1.0f;
		}
		else if( !strcmp( cristr, "HDBIC" ) ) {
			cri = HDBIC;
		}
		else if( !strcmp( cristr, "HDHQ" ) ) {
			cri = HDHQ;
		}
		else if( cristr[0] == 'E' && cristr[1] == 'B' && cristr[2] == 'I' && cristr[3] == 'C' ) {
			cri = EBIC;
			ebic_gamma = atof( cristr+4 );
		}
		else {
			printf( "There is no criterion named '%s'!\n", cristr );
			exit( 1 );
		}

		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d", &nT );
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d", &nthr );

		// Close file
		fclose( file );

		printf( "Loaded config from '%s'.\n", fileroot );
	}
	else {
		printf( "Unable to open file '%s'!\n", fileroot );

		// Open file
		file = fopen( fileroot, "w" );
		if( !file ) {
			printf( "Unable to create file '%s'!\n", fileroot );
			exit( 1 );
		}
		printf( "Creating config file '%s'...\n", fileroot );

		// Write data
		fprintf( file, "nP/nI %d %d\n", par.nP, par.nI );
		fprintf( file, "prob  %.2f %.2f %.2f %.2f %.2f\n", par.pfg, par.pfl, par.pfr, par.pbl, par.pbr );
		switch(cri) {
		case AIC: 
			fprintf( file, "cri   AIC\n" );
			break;
		case BIC:
			fprintf( file, "cri   BIC\n" );
			break;
		case EBIC:
			fprintf( file, "cri   EBIC%.1f\n", ebic_gamma );
			break;
		case HDBIC:
			fprintf( file, "cri   HDBIC\n" );
			break;
		case HDHQ:
			fprintf( file, "cri   HDHQ\n" );
			break;
		}
		fprintf( file, "nT    %d\n", nT );
		fprintf( file, "nthr  %d\n", nthr );

		fprintf( file, "\n\nNote:\n" );
		fprintf( file, "<nP>:   the number of particles.\n" );
		fprintf( file, "<nI>:   the number of iterations.\n" );
		fprintf( file, "<prob>: <pfg> <pfl> <pfr> <pbl> <pbr>\n" );
		fprintf( file, "<pfg>:  the probability for forward step: global\n" );
		fprintf( file, "<pfl>:  the probability for forward step: local\n" );
		fprintf( file, "<pfr>:  the probability for forward step: random\n" );
		fprintf( file, "<pbl>:  the probability for backward step: local\n" );
		fprintf( file, "<pbr>:  the probability for backward step: random\n" );
		fprintf( file, "<cri>:  the criterion.\n" );
		fprintf( file, "        AIC:         Akaike information criterion.\n" );
		fprintf( file, "        BIC:         Bayesian information criterion.\n" );
		fprintf( file, "        EBIC:        Extended Bayesian information criterion.\n" );
		fprintf( file, "        EBIC<gamma>: EBIC with parameter gamma.\n" );
		fprintf( file, "        HDBIC:       High-dimensional Bayesian information criterion.\n" );
		fprintf( file, "        HDHQ:        High-dimensional Hannan-Quinn information criterion.\n" );
		fprintf( file, "<nT>    the number of tests.\n" );
		fprintf( file, "<nthr>  the number of threads.\n" );
		fprintf( file, "        0: uses system default.\n" );

		// Close file
		fclose( file );

		printf( "Created config file '%s'.\n", fileroot );
		printf( "Uses default config.\n" );
	}
}


/**
 * Load model from file
 *
 * @param fileroot the root of model file
 */
void pass_load( const char* fileroot ) {
	FILE *file;
	int size;

	printf( "Loading model from '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "rb" );
	if( !file ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Read data
	fread( &size, sizeof(int), 1, file );
	dataname = (char*) malloc( sizeof(char) * size );
	fread( dataname, sizeof(char), size, file );
	fread( &n, sizeof(int), 1, file );
	fread( &p, sizeof(int), 1, file );
	X = (float*) malloc( sizeof(float) * n * p );
	Y = (float*) malloc( sizeof(float) * n );
	J = (int*) malloc( sizeof(int) * p );
	fread( X, sizeof(float), n * p, file );
	fread( Y, sizeof(float), n, file );
	fread( J, sizeof(int), p, file );

	// Close file
	fclose( file );

	printf( "Loaded model from '%s'.\n", fileroot );
}


/**
 * Allocate memory
 *
 * @param data the data
 */
void pass_malloc_data( struct Data* data ) {
	data->X      = (float*) malloc( sizeof(float) * n * n );
	data->Y      = (float*) malloc( sizeof(float) * n );
	data->Beta   = (float*) malloc( sizeof(float) * n );
	data->Theta  = (float*) malloc( sizeof(float) * n );
	data->Eta    = (float*) malloc( sizeof(float) * n );
	data->P      = (float*) malloc( sizeof(float) * n );
	data->W      = (float*) malloc( sizeof(float) * n );
	data->M      = (float*) malloc( sizeof(float) * n * (n+1) / 2 );

	data->Idx_lf = (int*) malloc( sizeof(int) * n );
	data->Idx_fl = (int*) malloc( sizeof(int) * (p+1) );
	data->I      = (int*) malloc( sizeof(int) * p );
	data->I_best = (int*) malloc( sizeof(int) * p );

	data->Ftemp  = (float*) malloc( sizeof(float) * n );
	data->Itemp  = (int*) malloc( sizeof(int) * p );
}


/**
 * Free memory
 *
 * @param data the data
 */
void pass_free_data( struct Data* data ) {
	free( data->X );
	free( data->Y );
	free( data->Beta );
	free( data->Theta );
	free( data->Eta );
	free( data->P );
	free( data->W );
	free( data->M );

	free( data->Idx_lf );
	free( data->Idx_fl );
	free( data->I );
	free( data->I_best );

	free( data->Ftemp );
	free( data->Itemp );
}


/**
 * PaSS main function
 *
 * @param data the data list
 */
void pass_main( struct Data* data ) {
	int tid, i, j, k, l;

	// Use OMP parallel
	#pragma omp parallel private( tid, i, j, k, l ) num_threads( nthr )
	{
		tid = omp_get_thread_num();

		for( j = tid; j < par.nP; j+=nthr ) {
			// Initialize particles
			pass_init_model( data+j );
			pass_update_model( data+j, rand() % p );
			pass_compute_beta( data+j );
			pass_compute_cri( data+j );
			data[j].phi_old= data[j].phi;

			// Copy best model
			data[j].phi_best = data[j].phi;
			memcpy( data[j].I_best, data[j].I, sizeof(int) * p );
		}

		#pragma omp barrier

		// Find best data
		for( i = 1; i < par.nI; i++ ) {
			for( j = tid; j < par.nP; j+=nthr ) {
				// Update model
				pass_select_fb( data+j );
				pass_compute_cri( data+j );

				// Change status
				if( data[j].phi > data[j].phi_old ) {
					data[j].stat = !data[j].stat;
				}
				if( data[j].k <= 1 ) {
					data[j].stat = 1;
				}
				if( data[j].k >= n-2 || data[j].k >= p-4 ) {
					data[j].stat = 0;
				}

				data[j].phi_old = data[j].phi;

				// Copy best model
				for( k = par.nP-2; k < par.nP+2; k++ ) {
					l = (j+k) % par.nP;
					if( data[l].phi_best > data[j].phi ) {
						data[l].phi_best = data[j].phi;
						memcpy( data[l].I_best, data[j].I, sizeof(int) * p );
					}
				}
			}
		}
	}
}


/**
 * Initialize the model
 *
 * @param data the initilizing data
 */
void pass_init_model( struct Data* data ) {
	int i, ione = 1;

	data->k = 0;

	// Initialize index
	for( i = 0; i < p; i++ ) {
		data->I[i] = 0;
	}

	// X[0 col] := 1.0
	// Ones := 1.0
	data->Ones = data->X;
	for( i = 0; i < n; i++ ) {
		data->Ones[i] = 1.0f;
	}

	// Copy Y
	scopy( &n, Y, &ione, data->Y, &ione );

	// Set status
	data->stat = 1;
}


/**
 * Select index to add or remove
 * 
 * @param data the updating data
 */
void pass_select_fb( struct Data* data ) {
	int i, j, idx = -1, choose, ione = 1, itemp;
	float *Xnew, beta, frand = (float)rand() / RAND_MAX, fone = 1.0f, fzero = 0.0f, ftemp, beta_temp, llv_temp = fNaN;

	if( data->stat ) { // Forward step
		// Itemp[0~itemp] := I(best) exclude I(local)
		// Itemp[0~(p-k)] := complement of I(local)
		for( i = 0, j = p-data->k, itemp = 0; i < p; i++ ) {
			if( !(data->I[i]) ) {
				if( data->I_best[i] ) {
					data->Itemp[itemp] = i;
					itemp++;
				}
				else {
					j--;
					data->Itemp[j] = i;
				}
			}
		}

		if( itemp ) {
			choose = ( frand < par.pfg ) + ( frand < par.pfg+par.pfl );
		}
		else {
			choose = frand < par.pfl / ( par.pfl + par.pfr );
		}

		switch( choose ) {
		case 2: // Global best
			idx = data->Itemp[rand() % itemp];
			break;
		case 1: // Local best
			for( i = 0; i < p; i++ ) {
				if( !(data->I[i]) ) {

					// beta := 0.0
					beta = 0.0f;

					// Xnew := X(full)[i col]
					Xnew = X+i*n;

					// ftemp := Xnew' * Y
					ftemp = sdot( &n, Xnew, &ione, data->Y, &ione );

					// Find beta using Newton-Raphson's method
					while(1) {

						// Ftemp := Theta_new := Theta + beta * Xnew
						scopy( &n, data->Theta, &ione, data->Ftemp, &ione );
						saxpy( &n, &beta, Xnew, &ione, data->Ftemp, &ione );

						// Ftemp := Eta_new := exp( Theta_new )
						vsExp( n, data->Ftemp, data->Ftemp );
						vsAdd( n, data->Ftemp, data->Ones, data->W );

						// Ftemp := P_new := Eta_new ./ ( 1+Eta_new )
						vsDiv( n, data->Ftemp, data->W, data->Ftemp );

						// W := W_new := P_new .* (1-P_new)
						vsDiv( n, data->Ftemp, data->W, data->W );

						// W := W_new .* Xnew
						vsMul( n, data->W, Xnew, data->W );

						// beta_temp := ( Xnew'*(Y-P_new) ) / ( Xnew'*diag(W_new)*Xnew )
						beta_temp = ( ftemp - sdot( &n, Xnew, &ione, data->Ftemp, &ione ) ) / sdot( &n, Xnew, &ione, data->W, &ione );

						// beta += beta_temp
						beta += beta_temp;

						// Check if converged
						if( !( beta_temp > 1e-4f ) ) {
							break;
						}
					}

					/*================================================================*
					 * ftemp := Y' * Theta_hat - sum( log( 1 + (eta_hat-1)*p ) )      *
					 *================================================================*/

					// Ftemp := Theta_hat := beta * Xnew
					saxpby( &n, &beta, Xnew, &ione, &fzero, data->Ftemp, &ione );

					// ftemp := Y' * Theta_hat
					ftemp = sdot( &n, data->Y, &ione, data->Ftemp, &ione );

					// Ftemp := log( 1 + (Eta_hat-1)*P )
					vsExpm1( n, data->Ftemp, data->Ftemp );
					vsMul( n, data->Ftemp, data->P, data->Ftemp );
					vsLog1p( n, data->Ftemp, data->Ftemp );

					// ftemp -= sum( Ftemp )
					ftemp -= sdot( &n, data->Ftemp, &ione, data->Ones, &ione );

					/*================================================================*/

					// Check if this value is maximum
					if( !(ftemp < llv_temp) ) {
						llv_temp = ftemp;
						idx = i;
					}
				}
			}
			break;
		case 0: // Random
			idx = data->Itemp[rand() % (p-data->k)];
			break;
		}
	}
	else { // backward step
		if( frand < par.pbl ) { // Local best
			for( i = 0; i < p; i++ ) {
				if( data->I[i] ) {

					/*================================================================*
					 * ftemp := Y' * Theta_hat - sum( log( 1 + (eta_hat-1)*p ) )      *
					 *================================================================*/

					// Ftemp := Theta_hat := -Beta(full)[i] * X(full)[i col]
					beta = -(data->Beta[data->Idx_fl[i]]);
					saxpby( &n, &beta, X+i*n, &ione, &fzero, data->Ftemp, &ione );

					// ftemp := Y' * Theta_hat
					ftemp = sdot( &n, data->Y, &ione, data->Ftemp, &ione );

					// Ftemp := log( 1 + (Eta_hat-1)*P )
					vsExpm1( n, data->Ftemp, data->Ftemp );
					vsMul( n, data->Ftemp, data->P, data->Ftemp );
					vsLog1p( n, data->Ftemp, data->Ftemp );

					// ftemp -= sum( Ftemp )
					ftemp -= sdot( &n, data->Ftemp, &ione, data->Ones, &ione );

					/*================================================================*/

					// Check if this value is maximum
					if( !(ftemp < llv_temp) ) {
						llv_temp = ftemp;
						idx = i;
					}
				}
			}
		}
		else { // Random
			idx = data->Idx_lf[rand() % data->k + 1];
		}
	}

	// Update model
	pass_update_model( data, idx );

	// Compute beta
	pass_compute_beta( data );
}


/**
 * Update the model
 *
 * @param data the updating data
 * @param idx the updating full index
 */
void pass_update_model( struct Data* data, const int idx ) {
	int i, ione = 1;

	if( data->stat ) { // forward step
		data->k++;

		// Update index
		data->I[idx] = 1;
		data->Idx_lf[data->k] = idx;
		data->Idx_fl[idx] = data->k;

		// Insert new row of X
		scopy( &n, X + idx*n, &ione, data->X + data->k*n, &ione );
	}
	else { // backward step
		// Update index
		data->I[idx] = 0;

		// Find index
		i = data->Idx_fl[idx];

		// Swap i to the end
		if( i != data->k ) {
			scopy( &n, data->X + data->k*n, &ione, data->X + i*n, &ione );
			data->Idx_lf[i] = data->Idx_lf[data->k];
			data->Idx_fl[data->Idx_lf[i]] = i;
		}

		data->k--;
	}
}


/**
 * Compute Beta
 *
 * @param data the updating data
 */
void pass_compute_beta( struct Data* data ) {
	int i, l, ione = 1, info;
	float fone = 1.0f, fzero = 0.0f;

	l = data->k+1;

	// Beta := 0.0f
	for( i = 0; i < l; i++ ) {
		data->Beta[i] = 0.0f;
	}
	
	// P := 0.5, W := 0.5
	for( i = 0; i < n; i++ ) {
		data->P[i] = 0.5f;
		data->W[i] = 0.25f;
	}

	// Find Beta using Newton-Raphson's method
	do {

		/*================================================================*
		 * Beta += inv( X' * diag(W) * X ) * X' * (Y-P)                   *
		 *================================================================*/

		// M := X' * diag(W) * X
		for( i = 0; i < l*(l+1)/2; i++ ) {
			data->M[i] = 0.0f;
		}
		for( i = 0; i < n; i++ ) {
			sspr( "L", &l, data->W+i, data->X+i, &n, data->M );
		}

		// Cholesky docomposition of M
		spptrf( "L", &l, data->M, &info );

		// W := (Y-P)
		vsSub( n, data->Y, data->P, data->W );

		// Ftemp := X' * W
		sgemv( "T", &n, &l, &fone, data->X, &n, data->W, &ione, &fzero, data->Ftemp, &ione );

		// Solve Ftemp := inv(M) * Ftemp
		stpsv( "L", "N", "N", &l, data->M, data->Ftemp, &ione );

		// Solve Ftemp := inv(M') * Ftemp
		stpsv( "L", "T", "N", &l, data->M, data->Ftemp, &ione );

		// Beta += Ftemp
		vsAdd( l, data->Beta, data->Ftemp, data->Beta );

		/*================================================================*/
		
		// Theta := X * Beta
		sgemv( "N", &n, &l, &fone, data->X, &n, data->Beta, &ione, &fzero, data->Theta, &ione );

		// Eta := exp( Theta )
		vsExp( n, data->Theta, data->Eta );

		// P := Eta ./ ( 1+Eta )
		vsAdd( n, data->Eta, data->Ones, data->W );
		vsDiv( n, data->Eta, data->W, data->P );

		// W := P .* (1-P)
		vsDiv( n, data->P, data->W, data->W );
	} while( snrm2( &l, data->Ftemp, &ione ) > sqrt( l ) * 1e-4f );
}


/**
 * Compute the value given by criterion
 *
 * @param data the updating data
 */
void pass_compute_cri( struct Data *data ) {
	int i, ione = 1;
	float fone = 1.0f, fzero = 0.0f, fnone = -1.0f;

	// llv := Y' * Theta - sum( log( 1+Eta ) )
	vsLog1p( n, data->Eta, data->Ftemp );
	data->llv = sdot( &n, data->Y, &ione, data->Theta, &ione ) - sdot( &n, data->Ftemp, &ione, data->Ones, &ione );

	// Compute criterion
	switch(cri) {
	case AIC:   // phi := -2llv + 2k
		data->phi = -2.0f * data->llv + 2.0f * data->k;
		break;
	case BIC:   // phi := -2llv + k * log(n)
		data->phi = -2.0f * data->llv + data->k * log( (float)n );
		break;
	case EBIC:  // phi := -2llv + k * log(n) + 2ebic_gamma * log(p choose k)
		data->phi = -2.0f * data->llv + data->k * log( (float)n ) + 2.0f * ebic_gamma * ( lgammaf( p+1.0f ) - lgammaf( p-data->k+1.0f ) - lgammaf( data->k+1.0f ) );
		break;
	case HDBIC: // phi := -2llv + k * log(n) * log(p)
		data->phi = -2.0f * data->llv + data->k * log( (float)n ) * log( (float)p );
		break;
	case HDHQ:  // phi := -2llv + 2.01k * log(log(n)) * log(p)
		data->phi = -2.0f * data->llv + 2.01f * data->k * log( log( (float)n ) ) * log( (float)p );
		break;
	}
}

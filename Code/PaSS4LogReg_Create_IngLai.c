/**
 * Particle Swarm Stepwise (PaSS) Algorithm for Logistic Regression
 * PaSS4LogReg_Create_IngLai.c
 * Create a logistic model using Ing and Lai's method
 */

/**@mainpage
 * @author Mu Yang
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mkl.h>
#include <omp.h>

#define passSeed time( NULL )


// Global variables
int n;         /**< scalar, the number of statistical units */
int p;         /**< scalar, the number of total effects */
int r;         /**< scalar, the number of using effects */
float *X;      /**< matrix, n by p, the regressors */
float *Y;      /**< vector, n by 1, the regressand */
float *Beta;   /**< vector, n by 1, the effects */
int *J;        /**< matrix, p by 1, the chosen indices (boolean), true solution */
char *dataname; /**< string, the name of data */

// Functions
void logreg_cfg( const char* );
void logreg_save( const char* );


/**
 * Main function
 */
int main( int argc, char** argv ) {
	int i, j, iseed[4] = { passSeed, passSeed, passSeed, 1 }, ione = 1, ithree = 3, itemp;
	float *Ftemp, fone = 1.0, fzero = 0.0, fdotfive = 0.5, ftemp;
	char *cfgroot, *dataroot;

	n = 400;
	p = 4000;
	r = 10;

	cfgroot = "PaSS4LogReg_Create_IngLai.cfg";
	dataroot = "PaSS4LogReg.dat";
	dataname = "LogReg_IngLai";

	if( argc > 1 ) cfgroot  = argv[1];
	if( argc > 2 ) dataroot = argv[2];

	// Load config
	logreg_cfg( cfgroot );

	printf( "Creating a logistic model using Ing and Lai's method...\n" );

	// Allocate memory
	X     = (float*) malloc( sizeof(float) * n * p );
	Y     = (float*) malloc( sizeof(float) * n );
	J     = (int*)   calloc( p, sizeof(int) );
	Ftemp = (float*) malloc( sizeof(float) * n );

	// Generate X using normal random
	// Generate Y using uniform random
	itemp = n*p;
	slarnv( &ithree, iseed, &itemp, X );
	slarnv( &ione, iseed, &n, Y );

	// Ftemp[i] := sum( X[i, 0~r] )
	for( i = 0; i < n; i++ ) {
		for( j = 0; j < r; j++ ) {
			Ftemp[i] += X[i+j*n];
		}
	}

	// X[i col] := sqrt( 3/4r ) * Ftemp + 1/2 * X[i col]
	ftemp = sqrt( .75 / r );
	for( i = r; i < p; i++ ) {
		saxpby( &n, &ftemp, Ftemp, &ione, &fdotfive, X + i*n, &ione );
	}

	// Ftemp := X[(0~r) cols] * Beta
	sgemv( "N", &n, &r, &fone, X, &n, Beta, &ione, &fzero, Ftemp, &ione );

	// Ftemp := exp( Ftemp )
	vsExp( n, Ftemp, Ftemp );

	// Ftemp := Ftemp ./ (1+Ftemp)
	vsLinearFrac( n, Ftemp, Ftemp, fone, fzero, fone, fone, Ftemp );

	// Compute Y
	for( i = 0; i < n; i++ ) {
		Y[i] = ( Y[i] < Ftemp[i] );
	}

	// Generate J
	for( i = 0; i < r; i++ ) {
		J[i] = 1;
	}

	// Save data
	logreg_save( dataroot );

	// Free memory
	free( X );
	free( Y );
	free( J );
	free( Beta );
	free( Ftemp );

	return 0;
}


/**
 * Load config from file
 *
 * @param fileroot the root of config file
 */
void logreg_cfg( const char* fileroot ) {
	#define BUF_SIZE 1024
	FILE *file;
	int i, offset;
	char line[BUF_SIZE], *linetemp;

	printf( "Loading config from '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "r" );

	// Check if file exists
	if( file ) {
		// Read data
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d %d %d", &n, &p, &r );
		fgets( line, BUF_SIZE, file );
		Beta  = (float*) malloc( sizeof(float) * r );
		sscanf( line, "%*s %n", &offset );
		linetemp = line;
		for( i = 0; i < r; i++ ) {
			linetemp += offset;
			sscanf( linetemp, "%f %n", Beta+i, &offset );
		}

		// Close file
		fclose( file );

		printf( "Loaded config from '%s'.\n", fileroot );
	}
	else {
		printf( "Unable to open file '%s'!\n", fileroot );

		// Open file
		file = fopen( fileroot, "w" );
		if( !file ) {
			printf( "Unable to create file '%s'!\n", fileroot );
			exit( 1 );
		}
		printf( "Creating config file '%s'...\n", fileroot );
	
		// Generate Beta
		Beta  = (float*) malloc( sizeof(float) * r );
		for( i = 0; i < r; i++ ) {
			Beta[i] = -1.5 + .35*i;
		}

		// Write data
		fprintf( file, "n/p/r %d %d %d\n", n, p, r );
		fprintf( file, "Beta  %.3f", Beta[0] );
		for( i = 1; i < r; i++ ) {
			fprintf( file, " %.3f", Beta[i] );
		}
		fprintf( file, "\n" );

		// Close file
		fclose( file );

		printf( "Created config file '%s'.\n", fileroot );
		printf( "Uses default config.\n" );
	}

	// Display config
	printf( "\nn = %d, p = %d, r = %d\n", n, p, r );
	printf( "Beta = " );
	for( i = 0; i < r; i++ ) {
		printf( "%8.3f", Beta[i] );
	}
	printf( "\n\n" );
}


/**
 * Save model into file
 *
 * @param fileroot the root of model file
 */
void logreg_save( const char* fileroot ) {
	FILE *file;
	int size = strlen( dataname ) + 1;

	printf( "Saving model into '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "wb" );
	if( !file ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Write data
	fwrite( &size, sizeof(int), 1, file );
	fwrite( dataname, sizeof(char), size, file );
	fwrite( &n, sizeof(int), 1, file );
	fwrite( &p, sizeof(int), 1, file );
	fwrite( X, sizeof(float), n * p, file );
	fwrite( Y, sizeof(float), n, file );
	fwrite( J, sizeof(int), p, file );

	// Close file
	fclose( file );

	printf( "Saved model into '%s'.\n", fileroot );
}
